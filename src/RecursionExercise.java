public class RecursionExercise {

    public void decimalToBinary(int n) {
        if (n==1) {
            System.out.print(1);
        } else {
            decimalToBinary(n/2);
            System.out.print(n%2);
        }
    }

    private static long power(int x, int n) {
        long y = 0;
        if (n == 0)
            return 1;
        else if (n == 1)
            return x;
        else {
            return x * power(x, n-1);
        }
    }

    public int gcdRecursion(int m, int n) {
        if (n == 0) return m;
        else return gcdRecursion(n, m % n);
    }

    public void permutation(String str) {
        permutation("", str);
    }

    private void permutation(String prefix, String str) {
        int n = str.length();
        if (n == 0) System.out.println(prefix);
        else {
            for (int i = 0; i < n; i++)
                permutation(prefix + str.charAt(i), str.substring(0, i) + str.substring(i+1, n));
        }
    }

    public static void main(String[] args) {
        RecursionExercise ex = new RecursionExercise();
        ex.decimalToBinary(1024);
        System.out.println();
        System.out.println(ex.power(2,10));
        ex.permutation("abcd");
    }

}
