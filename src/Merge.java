/**
 * Given two sorted arrays A and B, and A has a buffer that is enough to contain B,
 * merge the two arrays into a single array.
 */

public class Merge {

    public static void main(String[] args) {

        int[] A = {1, 10, 11, 15, 20, 30, 0, 0, 0, 0, 0, 0, 0, 0};
        int[] B = {2, 3, 4, 8, 10, 25};
        merge(A, B, 6);
        for (int i = 0; i < A.length; i++) {
            System.out.print(A[i] + " ");
        }
    }

    private static void merge(int[] A, int[] B, int lengthA) {
        int i = lengthA - 1;
        int j = B.length - 1;
        int k = A.length - 1;

        while (i>=0 && j>=0) {
            if (A[i] < B[j]) {
                A[k] = B[j];
                j--;
            } else {
                A[k] = A[i];
                i--;
            }
            k--;
        }

        // put the remaining A elements in place
        while (i>=0) {
            A[k] = A[i];
            k--;
            i--;
        }

        // put the remaining B elements in place
        while (j>=0) {
            A[k] = B[j];
            k--;
            j--;
        }

        if (lengthA + B.length < A.length) {
            for (int n = 0; n < A.length - B.length - lengthA; n++) {
                A[n] = Integer.MIN_VALUE;
            }
        }
    }

}
