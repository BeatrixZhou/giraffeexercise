package StringStack;

public class textStringStack {

    public static void main(String[] args) {

        StringStackLinkedList testStack = new StringStackLinkedList();
        System.out.println(testStack.isEmpty());
        testStack.push("cozy");
        testStack.push("and");
        testStack.push("cute");
        testStack.push("very");
        testStack.push("are");
        testStack.push("giraffes");
        testStack.push("indeed");
        testStack.push("code");
        testStack.push("good");
        testStack.push("very");
        testStack.push("is");
        testStack.push("it");
        testStack.push("god");
        testStack.push("my");
        testStack.push("oh");
        testStack.printAllFromBottom();
        testStack.printAllFromTop();
        System.out.println(testStack.pop());
        System.out.println(testStack.pop());
        System.out.println(testStack.pop());

    }

}
