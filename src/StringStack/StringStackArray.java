package StringStack;

import java.util.NoSuchElementException;

public class StringStackArray<T extends String> {

    private int size = 10;
    private String[] underlyingArray;
    private int cursorPosition;

    public StringStackArray() {
        underlyingArray = new String[size];
        cursorPosition = 0;
    }

    public String pop() {
        if (isEmpty()) {
            throw new NoSuchElementException("Stack underflow");
        }
        String topElement = underlyingArray[cursorPosition-1];
        cursorPosition--;
        return topElement;
    }

    public void push(String s) {
        if (isFull()) {
            size *= 2;
            String[] tempArray = new String[size];
            for (int i=0; i<size/2; i++) {
                tempArray[i] = underlyingArray[i];
            }
            underlyingArray = tempArray;
        }
        underlyingArray[cursorPosition] = s;
        cursorPosition++;
    }

    public boolean isEmpty() {
        return cursorPosition==0;
    }

    private boolean isFull() {
        return cursorPosition==underlyingArray.length;
    }

    public void printAllFromTop() {
        for (int i=cursorPosition-1; i>=0; i--) {
            System.out.println(underlyingArray[i]);
        }
    }

    public void printAllFromBottom() {
        for (int i=0; i<cursorPosition; i++) {
            System.out.println(underlyingArray[i]);
        }
    }

    public int size() {
        return cursorPosition;
    }

}
