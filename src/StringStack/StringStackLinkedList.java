package StringStack;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;

public class StringStackLinkedList<T extends String> {

    private LinkedList<String> underlyingList;

    public StringStackLinkedList() {
        underlyingList = new LinkedList<>();
    }

    public String pop() {
        if (isEmpty()) {
            throw new NoSuchElementException("Stack underflow");
        }
        return underlyingList.removeLast();
    }

    public void push(String s) {
        underlyingList.addLast(s);
    }

    public boolean isEmpty() {
        return underlyingList.size()==0;
    }

    public void printAllFromTop() {
        int i = underlyingList.size() - 1;
        while (i >= 0) {
            System.out.println(underlyingList.get(i));
            i--;
        }
    }

    public void printAllFromBottom() {
        Iterator Iterator = underlyingList.iterator();
        while (Iterator.hasNext()) {
            System.out.println(Iterator.next());
        }
    }

}
