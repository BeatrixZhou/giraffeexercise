package RMIExample.common;

import java.rmi.*;

public interface MathsInterface extends Remote {

    public int add2(int x, int y) throws RemoteException;
    public int add3(int x, int y) throws RemoteException;
    public int add(int x, int y) throws RemoteException;
    public int sub(int x, int y) throws RemoteException;
    public int mul(int x, int y) throws RemoteException;
    public int div(int x, int y) throws RemoteException;
    public boolean guess(int x) throws RemoteException;
    public int C2F(int Ctemp) throws RemoteException;
    public int F2C(int Ctemp) throws RemoteException;

}
