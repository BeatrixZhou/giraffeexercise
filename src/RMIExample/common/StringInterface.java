package RMIExample.common;

import java.rmi.*;

public interface StringInterface extends Remote {

    public String miao() throws RemoteException;

}
