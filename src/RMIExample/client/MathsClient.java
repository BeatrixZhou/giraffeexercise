package RMIExample.client;

import RMIExample.common.MathsInterface;
import RMIExample.common.StringInterface;

import java.rmi.*;

public class MathsClient {

    public static void main (String[] argv) {
        try {
            // InetAddress localhost = InetAddress.getByName("localhost");
            // if it's not localhost, use //192.168.1.33/OurMathsService
            MathsInterface MathsObj = (MathsInterface) Naming.lookup ("OurMathsService");
            StringInterface StringObj = (StringInterface) Naming.lookup ("OurStringService");
            System.out.println ("The sum of 90 and 6 is " + MathsObj.add(90, 6));
            System.out.println(StringObj.miao());

            /*int i = 0;
            int x;
            int y = 1;
            while (y > 0) {
                x = (int) (1 + 9 * Math.random());
                if (MathsObj.guess(x)) break;
                i++;
            }
            System.out.println("I caught you after " + i + " attempts!!!");*/
        } catch (Exception e) {
            System.out.println ("MathsClient exception: " + e);
        }
    }

}

