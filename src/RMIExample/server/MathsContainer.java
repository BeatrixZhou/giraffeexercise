package RMIExample.server;

import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.*;

public class MathsContainer {

    public static void main (String[] argv) {
        try {
            // System.setProperty("java.rmi.server.hostname","192.168.27.1");
            // System.setSecurityManager(new java.rmi.RMISecurityManager());

            Registry registry = LocateRegistry.createRegistry(1099);

            Naming.rebind ("OurMathsService", new MathsServiceImpl());
            Naming.rebind("OurStringService", new StringServiceImpl());
            System.out.println ("Maths server is ready.");
            System.out.println("String server is ready.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
