package RMIExample.server;

import RMIExample.common.MathsInterface;

import java.rmi.*;
import java.rmi.server.*;

public class MathsServiceImpl extends UnicastRemoteObject implements MathsInterface {

    public MathsServiceImpl() throws RemoteException {
        super();
    }

    public int add2(int x, int y) {
        System.out.println("Invocation to ADD2 completed succesfully!");
        return (x+2*y);
    }
    public int add3(int x, int y) {
        System.out.println("Invocation to ADD2 completed succesfully!");
        return (x+2*y);
    }
    public int add(int x, int y) {
        System.out.println("Invocation to ADD completed succesfully!");
        return (x+y);
    }
    public int sub(int x, int y) {
        System.out.println("Invocation to SUB completed succesfully!");
        return (x-y);
    }
    public int mul(int x, int y) {
        System.out.println("Invocation to MUL completed succesfully!");
        return (x*y);
    }
    public int div(int x, int y) {
        System.out.println("Invocation to MUL completed succesfully!");
        return (x/y);
    }
    public boolean guess(int x) {
        int y = (int) (9 * Math.random()) + 1;
        if (x == y) return true;
        return false;
    }
    public int C2F(int Ctemp) {
        int Ftemp = (int) ((Ctemp * 1.8) + 32);
        return Ftemp;
    }
    public int F2C(int Ftemp) {
        int Ctemp = (int) ((Ftemp -32) / 1.8);
        return Ctemp;
    }

}

