package RMIExample.server;

import RMIExample.common.StringInterface;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class StringServiceImpl extends UnicastRemoteObject implements StringInterface {

    public StringServiceImpl() throws RemoteException {
        super();
    }

    public String miao() {
        System.out.println("Invocation to MIAO completed succesfully!");
        return "miao!";
    }

}
