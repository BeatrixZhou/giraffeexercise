package Backtracking;

/**
 * A very cute little rat in a maze full of cheese.
 * After looting all the cheese the cute little rat needs to get out of the maze.
 */
public class RatInMaze {

    private int[][] maze;
    private int mazeSize;
    private int[][] route;    // Use 1 to represent route

    public RatInMaze(int[][] maze) {
        this.maze = maze;
        mazeSize = maze.length;
        route = new int[mazeSize][mazeSize];
        route[0][0] = 1;
    }

    private boolean findExit(int entranceRow, int entranceColumn) {
        // Up
        if (entranceRow - 1 >= 0 && maze[entranceRow - 1][entranceColumn] == 1 && route[entranceRow - 1][entranceColumn] == 0) {
            route[entranceRow - 1][entranceColumn] = 1;
            if ((entranceRow - 1 == mazeSize - 1 && entranceColumn == mazeSize - 1) || findExit(entranceRow - 1, entranceColumn)) {
                return true;
            } else {
                route[entranceRow - 1][entranceColumn] = 0;
            }
        }
        // Down
        if (entranceRow + 1 < mazeSize && maze[entranceRow + 1][entranceColumn] == 1 && route[entranceRow + 1][entranceColumn] == 0) {
            route[entranceRow + 1][entranceColumn] = 1;
            if ((entranceRow + 1 == mazeSize - 1 && entranceColumn == mazeSize - 1) || findExit(entranceRow + 1, entranceColumn)) {
                return true;
            } else {
                route[entranceRow + 1][entranceColumn] = 0;
            }
        }
        // Left
        if (entranceColumn - 1 >= 0 && maze[entranceRow][entranceColumn - 1] == 1 && route[entranceRow][entranceColumn - 1] == 0) {
            route[entranceRow][entranceColumn - 1] = 1;
            if ((entranceRow == mazeSize - 1 && entranceColumn - 1 == mazeSize - 1) || findExit(entranceRow, entranceColumn - 1)) {
                return true;
            } else {
                route[entranceRow][entranceColumn - 1] = 0;
            }
        }
        // Right
        if (entranceColumn + 1 < mazeSize && maze[entranceRow][entranceColumn + 1] == 1 && route[entranceRow][entranceColumn + 1] == 0) {
            route[entranceRow][entranceColumn + 1] = 1;
            if ((entranceRow == mazeSize - 1 && entranceColumn + 1 == mazeSize - 1) || findExit(entranceRow, entranceColumn + 1)) {
                return true;
            } else {
                route[entranceRow][entranceColumn + 1] = 0;
            }
        }

        return false;
    }

    private void printRoute() {
        for (int i=0; i<mazeSize; i++) {
            for (int j=0; j<mazeSize; j++) {
                System.out.print(route[i][j]);
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        int[][] maze = {{1, 0, 0, 0, 0},
                        {1, 1, 1, 0, 1},
                        {1, 0, 1, 1, 0},
                        {1, 0, 1, 1, 1},
                        {1, 1, 1, 0, 1}};
        RatInMaze ratInMaze = new RatInMaze(maze);
        ratInMaze.findExit(0, 0);
        ratInMaze.printRoute();
    }

}
