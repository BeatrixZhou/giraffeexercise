package Backtracking;

import java.util.ArrayList;
import java.util.List;

/**
 * The classic N-Queen problem.
 */
public class NQueen {

    private int[][] board;    // Records how many queens are attacking position represented by int[row][column]
    private int nrOfQueens;
    private List<String> queens;

    public NQueen(int n) {
        nrOfQueens = n;
        board = new int[nrOfQueens][nrOfQueens];
        queens = new ArrayList<>();    // Used to keep track of positions of queens, for printing out
    }

    private boolean placeQueens(int row) {
        for (int column = 0; column < nrOfQueens; column++) {
            // If the spot is not attacked, tentatively place the queen at the spot
            if (board[row][column] == 0) {
                queens.add("(" + row + ", " + column + ")");
                // If it is the last row, it means the program has done its job
                if (row == nrOfQueens-1) {
                    return true;
                } else {
                    markAttackedSpots(row, column);
                    if (placeQueens(row + 1)) {
                        return true;
                    } else {
                        // Remove the last queen in the list and unmark all new attacked positions
                        queens.remove(queens.size()-1);
                        unmarkSpots(row, column);
                    }
                }
            }
        }
        return false;

    }

    private void markAttackedSpots(int row, int column) {
        // Mark self spot as attacked
        board[row][column]++;
        // Mark same row as attacked
        for (int i=0; i<nrOfQueens; i++) {
            if (i!=column) {
                board[row][i]++;
            }
        }
        // Mark same column as attacked
        for (int i=0; i<nrOfQueens; i++) {
            if (i!=row) {
                board[i][column]++;
            }
        }
        // Mark diagonal as attacked
        for (int i=0; i<nrOfQueens; i++) {
            if (row - i >= 0 && column - i >= 0) {
                board[row-i][column-i]++;
            }
            if (row - i >=0 && column + i < nrOfQueens) {
                board[row-i][column+i]++;
            }
            if (row + i < nrOfQueens && column - i >= 0) {
                board[row+i][column-i]++;
            }
            if (row + i < nrOfQueens && column + i < nrOfQueens) {
                board[row+i][column+i]++;
            }
        }
    }

    private void unmarkSpots(int row, int column) {
        // Mark self spot as attacked
        board[row][column]--;
        // Mark same row as attacked
        for (int i=0; i<nrOfQueens; i++) {
            if (i!=column) {
                board[row][i]--;
            }
        }
        // Mark same column as attacked
        for (int i=0; i<nrOfQueens; i++) {
            if (i!=row) {
                board[i][column]--;
            }
        }
        // Mark diagonal as attacked
        for (int i=0; i<nrOfQueens; i++) {
            if (row - i >= 0 && column - i >= 0) {
                board[row-i][column-i]--;
            }
            if (row - i >=0 && column + i < nrOfQueens) {
                board[row-i][column+i]--;
            }
            if (row + i < nrOfQueens && column - i >= 0) {
                board[row+i][column-i]--;
            }
            if (row + i < nrOfQueens && column + i < nrOfQueens) {
                board[row+i][column+i]--;
            }
        }
    }

    public static void main(String[] args) {
        NQueen nQueen = new NQueen(4);
        nQueen.placeQueens(0);
        System.out.println(nQueen.queens);
    }

}
