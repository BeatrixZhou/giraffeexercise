package Backtracking;

import java.util.ArrayList;
import java.util.List;

/**
 * The classic Knight Tour problem.
 */
public class KnightTour {

    private boolean[][] board;
    private List<String> tour;
    private int[][] moves = {{-2, 1}, {-1, 2}, {1, 2}, {2, 1}, {2, -1}, {1, -2}, {-1, -2}, {-2, -1}};

    public KnightTour() {
        board = new boolean[8][8];
        tour = new ArrayList<>();
    }

    private boolean generateTour(int startRow, int startColumn) {
        board[startRow][startColumn] = true;
        tour.add("(" + startRow + ", " + startColumn + ")");
        if (tour.size()==64) {
            return true;
        }
        for (int i=0; i<moves.length; i++) {
            if (startRow + moves[i][0] < 0 || startRow + moves[i][0] >= 8
                    || startColumn + moves[i][1] < 0 || startColumn + moves[i][1] >= 8) {
                continue;
            } else if (board[startRow + moves[i][0]][startColumn + moves[i][1]]) {
                continue;
            } else {
                if (!generateTour(startRow + moves[i][0], startColumn + moves[i][1])) {
                    board[startRow + moves[i][0]][startColumn + moves[i][1]] = false;
                    tour.remove(tour.size()-1);
                    continue;
                } else {
                    return true;
                }
            }
        }
        return false;
    }

    private void printSteps() {
        for (int i=0; i<64; i++) {
            System.out.print(tour.get(i) + " ");
        }
    }

    public static void main(String[] args) {
        KnightTour knightTour = new KnightTour();
        knightTour.generateTour(0, 0);
        knightTour.printSteps();
    }
}
