import java.util.ArrayList;
import java.util.Random;

/**
 * Given an array of bool and an array of integer with the same length,
 * compact away the integers whose original corresponding bool is false,
 * respecting the original ordering.
 * The compaction should be in place and the false integers can be cleared
 * out from the array.
 */
public class InPlaceExtractArray {

    private ArrayList<Integer> data;
    private ArrayList<Boolean> positions;
    private int length;

    public InPlaceExtractArray(int length, int maxNumber) {
        data = new ArrayList<>();
        positions = new ArrayList<>();
        Random random = new Random();
        this.length = length;
        for (int i = 0; i < length; i++) {
            data.add(random.nextInt(maxNumber) + 1);
            positions.add(random.nextBoolean());
        }
    }

    public ArrayList<Integer> extractData() {
        for (int i = 0; i < length; i++) {
            if (!positions.get(i)) {
                data.set(i, null);
            }
        }
        System.out.println("Extracted data: " + data);
        int currentNullIndex = 0;
        for (int i = 0; i < length; i++) {
            Integer currentNumber = data.get(i);
            if (currentNumber != null) {
                data.set(currentNullIndex, currentNumber);
                currentNullIndex++;
            }
        }
        return data;
    }

    public static void main(String[] args) {
        InPlaceExtractArray inPlaceExtractArray = new InPlaceExtractArray(10, 100);
        System.out.println("Original data: " + inPlaceExtractArray.data);
        System.out.println("Positions: " + inPlaceExtractArray.positions);
        System.out.println("Result: " + inPlaceExtractArray.extractData());
    }

}
