package networkapplicationslab1;

import java.io.*;
import java.net.*;
import java.util.Date;

public class EchoServerNew {

    private static final int PORT = 1234;

    // single thread TCP
    public static void main(String[] args) {
        String host = "localhost";
        PrintWriter out = null;
        BufferedReader in = null;
        try {
            while (true) {
                ServerSocket s = new ServerSocket(PORT);
                // waiting for connection
                System.out.println("Waiting for TCP connection request ...");
                Socket connection = s.accept();
                System.out.println("Connection request is accepted from " + connection.getInetAddress().getHostAddress()
                        + " on port " + connection.getPort());

                in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                out = new PrintWriter(connection.getOutputStream(), true);

                // read data from client
                String msg = in.readLine();
                // close server
                if (msg.equals("bye")) {
                    System.out.println("Connection closed");
                    out.println("Thank you for using the echo service!");
                } else if (msg.equals("DATE()")) {
                    System.out.println("Command received from client: " + msg);
                    out.println(new Date().toString());
                    System.out.println("Current date and time sent to client");
                } else if (msg.startsWith("DLOAD")) {
                    String fileName = msg.substring(6);
                    sendFile(fileName, connection);
                } else {
                    System.out.println("Unknown command received from client: " + msg);
                    // send data to client
                    out.println(msg);
                    System.out.println("Unknown command sent back to client: " + msg);
                }
                connection.close();
                s.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // single thread UDP
    /*public static void main(String arg[]) throws Exception {

        byte[] receivedBuffer;
        byte[] sentBuffer;
        try {
            DatagramSocket serverSocket = new DatagramSocket(PORT);
            System.out.println("UDP Serving running ...");

            while (true) {
                receivedBuffer = new byte[1024];

                // get info of client
                DatagramPacket receivedPacket = new DatagramPacket(receivedBuffer, receivedBuffer.length);
                System.out.println ("Waiting for datagram packet");
                serverSocket.receive(receivedPacket);
                InetAddress clientAddress = receivedPacket.getAddress();
                int clientPort = receivedPacket.getPort();

                // read command from client
                byte[] data = new byte[receivedPacket.getLength()];
                System.arraycopy(receivedPacket.getData(), receivedPacket.getOffset(), data, 0, receivedPacket.getLength());
                String command = new String(data);
                System.out.println("Packet received from " + clientAddress + ": " + command);
                if (command.equals("bye")) {
                    System.out.println("Program quit");
                    break;
                } else if (command.equals("DATE()")) {
                    Date date = new Date();
                    String dateString = date.toString();
                    sentBuffer = dateString.getBytes();
                    DatagramPacket sentpacket = new DatagramPacket(sentBuffer, sentBuffer.length, clientAddress, clientPort);
                    serverSocket.send(sentpacket);
                    System.out.println("Current date and time sent to client");
                } else {
                    System.out.println("Unknown command received from client: " + command);
                    // send command back to client
                    sentBuffer = command.getBytes();
                    DatagramPacket sentpacket = new DatagramPacket(sentBuffer, sentBuffer.length, clientAddress, clientPort);
                    serverSocket.send(sentpacket);
                    System.out.println("Unknown command sent back to client: " + command);
                }

                // this is only needed when computer is not very powerful
                *//*try {
                    Thread.sleep(2000);
                } catch (InterruptedException ie){
                    ie.printStackTrace();
                }*//*

            }

        } catch (SocketException ex) {
            ex.printStackTrace();
        }

    }*/

    private static void sendFile(String fileName, Socket clientSocket) {

        BufferedInputStream bis = null;
        OutputStream os = null;
        long time;

        try {
            time = System.currentTimeMillis();
            File file = new File("/Users/xujia/Exercise/giraffeexercise/src/" + fileName);
            byte[] fileArray = new byte[(int) file.length()];
            bis = new BufferedInputStream(new FileInputStream(file));
            bis.read(fileArray, 0, fileArray.length);
            os = clientSocket.getOutputStream();
            System.out.println("Sending " + fileName + "(" + fileArray.length + " bytes)");
            os.write(fileArray, 0, fileArray.length);
            time = System.currentTimeMillis() - time;
            os.flush();
            System.out.println("Done, using " + time + " ms.");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bis != null) try {
                bis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (os != null) try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
