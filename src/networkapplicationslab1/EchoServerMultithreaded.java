/*
/ TCP multi-threading
 */

package networkapplicationslab1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

public class EchoServerMultithreaded {

    private final int PORT = 1234;
    ServerSocket serverSocket;

    public EchoServerMultithreaded() {
        try {
            serverSocket = new ServerSocket(PORT);
            while (true) {
                System.out.println("Waiting for connection requests ...");
                ClientSocket clientSocket = new ClientSocket(serverSocket.accept());
                new Thread(clientSocket).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class ClientSocket implements Runnable {

        Socket connection;
        BufferedReader in;
        PrintWriter out;

        ClientSocket(Socket s) {
            connection = s;
        }

        @Override
        public void run() {
            try {
                System.out.println("Connection request is accepted from " + connection.getInetAddress().getHostAddress()
                        + " on port " + connection.getPort());
                in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                out = new PrintWriter(connection.getOutputStream(), true);

                while (true) {
                    // read data from client
                    String msg;
                    while ((msg = in.readLine()) != null) {
                        if (msg.equals("DATE()")) {
                            System.out.println("Command received from client " + Thread.currentThread().getName() + ":" + msg);
                            out.println(new Date().toString());
                            System.out.println("Current date and time sent to client");
                        } else {
                            System.out.println("Unknown command received from client: " + Thread.currentThread().getName() + ":" + msg);
                            // send data to client
                            out.println(msg);
                            System.out.println("Unknown command sent back to client: " + msg);
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        EchoServerMultithreaded echoServer = new EchoServerMultithreaded();
    }

}
