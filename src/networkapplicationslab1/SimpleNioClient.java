package networkapplicationslab1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.SocketChannel;
import java.nio.channels.WritableByteChannel;

public class SimpleNioClient {

    public static void main(String[] args) {
        SocketAddress address = new InetSocketAddress("localhost", 1234);
        BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
        try {
            String line;
            while ((line = stdIn.readLine()) != null) {
                SocketChannel client = SocketChannel.open(address);
                byte[] message = line.getBytes();
                ByteBuffer buffer = ByteBuffer.wrap(message);
                client.write(buffer);
                System.out.println("Sending: " + line);
                buffer.clear();
                ByteBuffer bufferWriter = ByteBuffer.allocate(1024);
                WritableByteChannel out = Channels.newChannel(System.out);
                while (client.read(bufferWriter) != -1) {
                    bufferWriter.flip();
                    out.write(bufferWriter);
                    bufferWriter.clear();
                    System.out.println();
                }
                client.close();
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

}
