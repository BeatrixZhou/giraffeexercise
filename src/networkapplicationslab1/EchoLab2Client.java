package networkapplicationslab1;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author xujia
 */
public class EchoLab2Client {

    private static final int PORT = 1234;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // String fileName = "java.sun.com/index.html";    // too big
        // String fileName = "localhost:8080/HelloGiraffe/index.jsp"
        String host = "localhost";
        PrintWriter out = null;
        BufferedReader in = null;
        BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String fileName = "";
            String savedFileName = "";
            try {
                Socket s = new Socket(host, PORT);
                out = new PrintWriter(s.getOutputStream(), true);
                in = new BufferedReader(new InputStreamReader(s.getInputStream()));
                System.out.println("Connected to server " + s.getInetAddress().getHostAddress()
                        + " on port " + s.getPort());
                String line;
                if ((line = stdIn.readLine()) != null) {
                    if (line.equals("quit")) {
                        System.out.println("Program quit.");
                        break;
                    } else if (line.startsWith("DLOAD")) {
                        fileName = line.substring(6);
                        String tempName = fileName.replace(':', '-');
                        savedFileName = tempName.replace('/', '-');
                    }
                    out.println(line);
                    System.out.println("Sent to server: " + line);
                    String response = in.readLine();
                    if (response.startsWith("Send")) {
                        System.out.println("Receiving file ...");
                        saveFile(savedFileName, s);
                    } else {
                        System.out.println("Received from server: " + response);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(EchoLab2Client.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    if (in != null) {
                        in.close();
                    }
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void saveFile(String fileName, Socket clientSocket) {
        String downloadDirectory = "/Users/xujia/Downloads/NetworkApplicationsLab2Client/";
        int bytesRead;
        int count = 0;
        BufferedOutputStream bos = null;
        File fileToDownload = new File(downloadDirectory + fileName);
        if (fileToDownload.exists()) {
            System.out.println("File already exists!");
            return;
        }
        try {
            fileToDownload.createNewFile();
            bos = new BufferedOutputStream(new FileOutputStream(fileToDownload));
            byte[] buffer = new byte[1024];
            InputStream is = null;
            try {
                is = clientSocket.getInputStream();
                while ((bytesRead = is.read(buffer)) > 0) {
                    count += bytesRead;
                    bos.write(buffer, 0, bytesRead);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            bos.flush();
            System.out.println("File " + fileName
                    + " downloaded (" + count + " bytes read)");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (clientSocket != null) {
                try {
                    clientSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
