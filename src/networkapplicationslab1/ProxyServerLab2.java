package networkapplicationslab1;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author xujia
 */
public class ProxyServerLab2 {

    private static final int PORT = 1234;

    public static void main(String[] args) {
        String host = "localhost";
        PrintWriter out = null;
        BufferedReader in = null;
        try {
            ServerSocket s = new ServerSocket(PORT);

            while (true) {
                System.out.println("Waiting for TCP connection request ...");
                Socket connection = s.accept();
                System.out.println("Connection request is accepted from " + connection.getInetAddress().getHostAddress()
                        + " on port " + connection.getPort());
                in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                out = new PrintWriter(connection.getOutputStream(), true);

                // read data from client
                String msg;
                msg = in.readLine();
                if (msg != null) {
                    // close server
                    if (msg.equals("bye")) {
                        System.out.println("Connection closed");
                        out.println("Thank you for using the echo service!");
                        connection.close();
                        break;
                    } else if (msg.equals("DATE()")) {
                        System.out.println("Command received from client: " + msg);
                        out.println(new Date().toString());
                        System.out.println("Current date and time sent to client");
                    } else if (msg.startsWith("DLOAD")) {
                        String fileName = msg.substring(6);
                        String tempName = fileName.replace(':', '-');
                        String savedFileName = tempName.replace('/', '-');
                        if (checkFileExists("/Users/xujia/Downloads/" + savedFileName)) {
                            if (checkLocalFileLastModified("/Users/xujia/Downloads/" + savedFileName)) {
                                out.println("Sending file ...");
                                sendFile("/Users/xujia/Downloads/" + savedFileName, connection);
                            } else {
                                // delete local file
                                File file = new File("/Users/xujia/Downloads/" + savedFileName);
                                file.delete();
                                downloadFile("http://" + fileName, "/Users/xujia/Downloads/" + savedFileName);
                                out.println("Sending file ...");
                                sendFile("/Users/xujia/Downloads/" + savedFileName, connection);
                            }
                        } else {
                            if (checkDate("http://" + fileName)) {
                                if (checkLength("http://" + fileName)) {
                                    downloadFile("http://" + fileName, "/Users/xujia/Downloads/" + savedFileName);
                                    out.println("Sending file ...");
                                    sendFile("/Users/xujia/Downloads/" + savedFileName, connection);
                                } else {
                                    out.println("File is too big.");
                                }
                            } else {
                                out.println("File is too old.");
                            }
                        }
                    } else {
                        System.out.println("Unknown command received from client: " + msg);
                        // send data to client
                        out.println(msg);
                        System.out.println("Unknown command sent back to client: " + msg);
                    }
                }
            }
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static boolean checkDate(String webAddress) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date oldestDate = sdf.parse("2015-01-10");
            URL Url;
            URLConnection uCon = null;
            System.out.println("Creating URL ...." + webAddress);
            try {
                Url = new URL(webAddress);
                uCon = Url.openConnection();
                Date createdDate = new Date(uCon.getDate());
                if (createdDate.before(oldestDate)) {
                    System.out.println("File is too old.");
                    return false;
                } else {
                    return true;
                }
            } catch (MalformedURLException ex) {
                Logger.getLogger(ProxyServerLab2.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            } catch (IOException ex) {
                Logger.getLogger(ProxyServerLab2.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        } catch (ParseException ex) {
            Logger.getLogger(ProxyServerLab2.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private static boolean checkLength(String webAddress) {
        int length = 0;
        URL Url;
        URLConnection uCon = null;
        System.out.println("Creating URL ...." + webAddress);
        try {
            Url = new URL(webAddress);
            uCon = Url.openConnection();
            length = uCon.getContentLength();
            if (length > 16000) {
                System.out.println("File is too large.");
                return false;
            } else {
                return true;
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(ProxyServerLab2.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (IOException ex) {
            Logger.getLogger(ProxyServerLab2.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private static boolean checkFileExists(String fileName) {
        File file = new File(fileName);
        if (file.exists()) {
            return true;
        } else {
            return false;
        }
    }

    private static boolean checkLocalFileLastModified(String fileName) {
        File file = new File(fileName);
        long twoDays = 1000 * 60 * 60 * 24;
        System.out.println("Last modified: " + new Date(file.lastModified()));
        if (file.lastModified() + twoDays >= System.currentTimeMillis()) {
            return true;
        } else {
            return false;
        }
    }

    private static void downloadFile(String webAddress, String destination) {
        URL Url;
        URLConnection uCon = null;
        InputStream is = null;
        OutputStream outStream = null;
        byte[] buf;
        int ByteRead, ByteWritten = 0;
        boolean exists = false;
        long lastupdated = 0;
        System.out.println("Creating URL ...." + webAddress);
        try {
            Url = new URL(webAddress);
            File fileDir = new File(destination);
            exists = fileDir.exists();
            if (exists) {
                System.out.println("File already Exists! Download CANCELLED!");
                return;
            }
            outStream = new BufferedOutputStream(new FileOutputStream(destination));
            uCon = Url.openConnection();
            is = uCon.getInputStream();
            buf = new byte[2048];
            while ((ByteRead = is.read(buf)) != -1) {
                outStream.write(buf, 0, ByteRead);
                ByteWritten += ByteRead;
                if (System.currentTimeMillis() - lastupdated > 5000) {
                    System.out.println(ByteWritten + 1 + " bytes read...");
                    lastupdated = System.currentTimeMillis();
                }
            }
            System.out.print("\nDownload from URL Successful!\t");
            System.out.println("File: \"" + destination + "\"\n Number of bytes:  " + ByteWritten);
        } catch (Exception e) {
            System.out.println("\nDownload failed ...");
            e.printStackTrace();
            exists = true;
        } finally {
            try {
                if (!exists) {
                    is.close();
                    outStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void sendFile(String fileName, Socket clientSocket) {
        BufferedInputStream bis = null;
        OutputStream os = null;
        long time;
        try {
            time = System.currentTimeMillis();
            File file = new File(fileName);
            byte[] fileArray = new byte[(int) file.length()];
            bis = new BufferedInputStream(new FileInputStream(file));
            bis.read(fileArray, 0, fileArray.length);
            os = clientSocket.getOutputStream();
            System.out.println("Sending " + fileName + "(" + fileArray.length + " bytes)");
            os.write(fileArray, 0, fileArray.length);
            time = System.currentTimeMillis() - time;
            os.flush();
            System.out.println("Done.");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (clientSocket != null) {
                try {
                    clientSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
