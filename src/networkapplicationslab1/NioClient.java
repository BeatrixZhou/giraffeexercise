package networkapplicationslab1;

import java.io.*;
import java.net.*;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.util.Calendar;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.channels.FileChannel;

public class NioClient {

    public static void main(String args[]) {
        String serverIP = "127.0.0.1";
        Socket client = null;
        int serverPort = 1235;
        int portNumber = 1234;    // Default port number we are going to use
        if (args.length >= 1) {
            portNumber = Integer.parseInt(args[0]);
        }
        while (true) {
            try {
                String msg = "";
                // Create a client socket
                client = new Socket(serverIP, portNumber);
                System.out.println("Client socket is created " + client + "\n");
                // Create an output stream of the client socket
                OutputStream clientOut = client.getOutputStream();
                PrintWriter pw = new PrintWriter(clientOut, true);
                // Create an input stream of the client socket
                InputStream clientIn = client.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(clientIn));
                // Create BufferedReader for a standard input
                BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter name of file to download: ");
                // Read data from standard input device and write it
                // to the output stream of the client socket.
                msg = stdIn.readLine().trim();
                pw.println(msg);

                //**************** Read data from the input stream of the client socket using CHANNELS.
                String fileName = "/Users/xujia/Downloads/" + msg;
                SocketChannel sChannel = SocketChannel.open(new InetSocketAddress(serverIP, serverPort));
                sChannel.configureBlocking(true);
                FileOutputStream fos = null;
                FileChannel fc = null;
                try {
                    fos = new FileOutputStream(new File(fileName));
                    fc = fos.getChannel();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                    System.exit(-1);
                }
                ByteBuffer buffer = ByteBuffer.allocate(1024);
                int nbrBytesRead = 0;
                long bytesReceived = 0;
                int count = 0;

                //record the starting time
                int m1 = Calendar.getInstance().get(Calendar.MINUTE);
                int s1 = Calendar.getInstance().get(Calendar.SECOND);

                while (nbrBytesRead != -1) {
                    buffer.clear();
                    nbrBytesRead = sChannel.read(buffer);
                    //System.out.println(nbrBytesRead + ":" + count);
                    //count++;
                    bytesReceived += nbrBytesRead;
                    if (nbrBytesRead == -1) {
                        break;
                    }
                    buffer.flip();
                    fc.write(buffer);
                }
                System.out.println(bytesReceived);
                sChannel.close();
                fc.close();

                //record the finishing time
                int m2 = Calendar.getInstance().get(Calendar.MINUTE);
                int s2 = Calendar.getInstance().get(Calendar.SECOND);
                //calculate time used to download
                int diff = (m2 - m1) * 60 + (s2 - s1);
                System.out.println("time used to download: " + diff);

            } catch (IOException ie) {
                System.out.println("I/O error " + ie);
            }
        }
    }

}
