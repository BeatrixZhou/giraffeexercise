package networkapplicationslab1;

import java.io.*;
import java.net.*;
import java.util.Enumeration;

public class NetworkInfo {

    // The method below displays the names of the network interface devices used on this computer.
    // Not all of them are Internet friendly.
    // Could be real or virtual devices.
    static void listInterfaces() {
        Enumeration <NetworkInterface> nics;
        String nicstr = "";
        String tempAddress = "";
        byte[] hwAddress;
        NetworkInterface NIC;
        try {
            nics = NetworkInterface.getNetworkInterfaces();
            while (nics.hasMoreElements() ){
                NIC = nics.nextElement();
                nicstr = NIC.toString();
                hwAddress = NIC.getHardwareAddress();
                if (hwAddress != null) {
                    for (int i=0; i<hwAddress.length-1; i++)    // You need to make some HEX conversion here.
                        tempAddress += hwAddress[i] + ": ";
                    if (hwAddress.length > 0)
                        tempAddress += (byte) hwAddress[hwAddress.length-1];
                }
                System.out.println("\nNIC " + nicstr  + ", MAC Address = " + tempAddress);
                tempAddress = "";
            }
        }
        catch (Exception e) {
            System.out.println("Error listing available interfaces:\n" + e.toString());
        }
    }

    // The method below displays the IP addresses bound to different devices such as 'eth1', 'WLAN', etc.
    // as returned by the listInterfaces() method here
    static void getIPAddressOfNIC(String interfaceName) {
        Enumeration <InetAddress> inetenum;
        System.out.println("\nGetting information about local network interfaces " + interfaceName + " ...");
        try {
            NetworkInterface ni = NetworkInterface.getByName(interfaceName);
            if (ni == null) {
                System.err.println("No such interface: " + interfaceName );
            }
            else {
                inetenum = ni.getInetAddresses();
                if (!inetenum.hasMoreElements()) {
                    System.out.println("This device is not bound to any IP address.");
                    return;
                }
                while (inetenum.hasMoreElements()) {
                    System.out.println("IPAddress for " + interfaceName + ": "+ inetenum.nextElement().toString());
                }
            }
        }
        catch (SocketException ex) {
            System.err.println("Could not list sockets." );
        }
    }

    // The method below returns all the IP addresses bound to the local machine network interfaces.
    // Useful to know the mapping between the MAC and network layer addresses.
    static void listMyIPAddresses(){
        System.out.println("\nGetting network information on the local machine ...");
        try {
            InetAddress localhost = InetAddress.getLocalHost();
            InetAddress[] myIps = InetAddress.getAllByName(localhost.getCanonicalHostName());
            if (myIps != null && myIps.length > 0) {
                System.out.println("Full list of IP addresses:");
                for (int i = 0; i < myIps.length; i++) {
                    InetAddress me =  myIps[i];
                    System.out.println("Name = " + me.getHostName());
                    System.out.println("IP Address = " + me.getHostAddress());
                    // this is to show that the IP address are unsigned byte
                    byte[] quad = me.getAddress();
                    for (int j = 0; j < quad.length; j++)
                        System.out.print(quad[j] + ".");
                    System.out.println();
                }
            }

        } catch (UnknownHostException e) {
            System.err.println(e);
        }
    }

    //This method is useful to get the IP address of a host (such as a website)
    static void getIPAddressOfWebSite(String hostname) {
        try {
            System.out.println("\nGetting the IP address of " + hostname);
            InetAddress[] addresses = InetAddress.getAllByName(hostname);
            for (int i = 0; i < addresses.length; i++)
                System.out.println(addresses[i]);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    // try a socket connection
    static void makeTCPConnection() {
        try {
            Socket toGoogle = new Socket("www.google.com", 80);
        } catch (UnknownHostException ex) {    // DNS cannot resolve hostname
            System.err.println(ex);
        } catch (IOException ex) {    // socket cannot be opened for some other reason
            System.err.println(ex);
        }
    }

    static void scanLowPorts() {
        String host = "www.portquiz.net";
        for (int i=1; i<1024; i++) {
            try {
                Socket s = new Socket(host, i);
                System.out.println("There is a server on port " + i + " of " + host);
            } catch (UnknownHostException ex) {    // DNS cannot resolve hostname
                System.err.println(ex);
                break;
            } catch (IOException ex) {    // socket cannot be opened for some other reason
                System.err.println(ex);
            }
        }
    }

    // protected Socket(), Socket(SocketImpl impl) create sockets but do not connect, can be used for e.g. encryption

    static void scanHighPorts() {
        // if many sockets need to be opened to the same host, it is more efficient to convert the hostname to an InetAddress
        // and repeatedly use that InetAddress
        String host = "www.portquiz.net";
        try {
            InetAddress inetAddress = InetAddress.getByName(host);
            for (int i =1024; i<65536; i++) {
                try {
                    Socket socket = new Socket(inetAddress, i);
                    System.out.println("There is a server on port " + i + " of " + host);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    // this example uses SOCKS proxy server at myproxy.example.com to connect to the host login,ibiblio.org
    static void testProxy() {
        SocketAddress proxyAddress = new InetSocketAddress("myproxy.example.com", 1080);
        Proxy proxy = new Proxy(Proxy.Type.SOCKS, proxyAddress);
        Socket s = new Socket(proxy);
        SocketAddress remote = new InetSocketAddress("login.ibiblio.org", 25);
        try {
            s.connect(remote);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void getSocketInfo() {
        try {
            Socket s = new Socket("www.google.com", 80);
            InetAddress host = s.getInetAddress();
            System.out.println("Connected to remote host " + host);
            InetAddress localaddress = s.getLocalAddress();
            System.out.println("Connecting from local address " + localaddress);
            int port = s.getPort();
            System.out.println("Connected on remote port " + port);
            // local port is embedded in outbound IP packets along with local IP address
            int localport = s.getLocalPort();
            System.out.println("Connecting from local port " + localport);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // login.ibiblio.org will fail to connect because it runs on port 25 not 80
        String[] hosts = {"www.oreilly.com", "www.hkr.se", "www.google.com", "login.ibiblio.org"};
        for (int i=0; i<hosts.length; i++) {
            try {
                Socket s = new Socket(hosts[i], 80);
                System.out.println("Connected to " + s.getInetAddress() + " on port " + s.getPort()
                        + " from port " + s.getLocalPort() + " of " + s.getLocalAddress());
            } catch (UnknownHostException ex) {
                System.err.println("I can' find " + hosts[i]);
            } catch (SocketException ex) {
                System.err.println("Could not connect to " + hosts[i]);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }

    static void testDaytime() {
        String host = "time.nist.gov";
        // String host = "vision.poly.edu";
        try {
            Socket s = new Socket(host, 13);
            InputStream stream = s.getInputStream();
            StringBuffer time = new StringBuffer();
            int c;
            while ((c = stream.read()) != -1) {
                time.append((char) c);
            }
            String timeString = time.toString().trim();
            System.out.println("It is " + timeString + " at " + host + ".");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void testOutputStream() {
        Writer out = null;
        try {
            Socket s = new Socket("www.google.com", 80);
            OutputStream bufferedStream = new BufferedOutputStream(s.getOutputStream());
            out = new OutputStreamWriter(bufferedStream, "ASCII");
            out.write("GET / HTTP 1.0\r\n\r\n");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    //Try each of these methods to know about your computer's network settings by uncommenting the respective line.
    public static void main(String[] args){
        // scanLowPorts();
        // scanHighPorts();
        // testProxy();
        // getSocketInfo();
        // testDaytime();
        // listInterfaces();
        // getIPAddressOfNIC("en0");
        // listMyIPAddresses();
        // getIPAddressOfWebSite("www.hkr.se");
        // testOutputStream();

        try {
            Socket socket = new Socket("www.hkr.se", 80);
            System.out.println(socket.getInetAddress());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}