package networkapplicationslab1;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.*;

public class NioComparer {

//The following two methods copy a local file to another file on the same computer.
//Using the java.io and java.nio packages.
//The performance of the two packages can be compared.  You can try varying the size of the NIO buffer and observe the time
//try copying files of different sizes: 1KB, 100KB, 1MB, 10MB, 100MB, 500MB, etc.


    static private void fileCopierio(String source, String dest) {
        int n, i;
        long t;
        System.out.println("\n...Now copying file using IO...");
        try {
            FileInputStream fis = new FileInputStream(source);
            FileOutputStream fos = new FileOutputStream(dest);
            i = 0;
            t = System.currentTimeMillis();
            while ((n = fis.read()) != -1) {
                i++;    //fis.read();
                fos.write(n);
            }
            t = System.currentTimeMillis() - t;
            fis.close();
            fos.close();
            System.out.println("File copy from " + source + " to " + dest + " is complete (" + i + " bytes.) in " + t + " milli sec.\n\n");
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        //      catch (FileNotFoundException fnfe) { System.err.println("File is not found");}
    }

    static private void fileCopiernio(String source, String dest, int bufsz) {
        int n, i;
        long t;
        ByteBuffer buf = ByteBuffer.allocate(bufsz);
        String s = "";
        System.out.println("\n....Now copying file using NIO...");
        try {
            FileInputStream fis = new FileInputStream(source);
            FileChannel fic = fis.getChannel();
            FileOutputStream fos = new FileOutputStream(dest);
            FileChannel foc = fos.getChannel();
            i = 0;
            n = 0;
            int loop = 0;
            t = System.currentTimeMillis();
            while (i != -1) {
                buf.clear();
                i = fic.read(buf);
                if (i == -1) break;
                buf.flip();
                if (buf.limit() < buf.capacity()) s = s + "-" + i;
                foc.write(buf);
                n = n + i;
                loop++;
                if (loop < 20) s = s + "-" + i;
            }
            t = System.currentTimeMillis() - t;
            System.out.println("File copy from " + source + " to " + dest + " is complete (" + n + " bytes.) in " + t + " milli Sec.\n\n");
            System.out.println("s = " + s);
            foc.close();
            fic.close();
            fis.close();
            fos.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        //      catch (FileNotFoundException fnfe) { System.err.println("File is not found");}

    }

    public static void main(String[] args) {
        // TODO code application logic here
        String src = "C:\\Users\\meda\\Downloads\\programs\\LMFlashProgrammer_1613.zip";    //C:\\someSourceDirectory\\SomeSourceFile.src";
        String niodst = "C:\\Users\\meda\\Downloads\\programs\\LMFlashProgrammer_1613.niozip";    //"C:\\SomeDestinationDirectory\\SomeDestinationFile.dest";
        String iodst = "C:\\Users\\meda\\Downloads\\programs\\LMFlashProgrammer_1613.iozip";    //"C:\\SomeDestinationDirectory\\SomeDestinationFile.dest";

        fileCopiernio(src, niodst, 8192);
        fileCopierio(src, iodst);
    }
}

