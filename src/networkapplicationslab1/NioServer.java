package networkapplicationslab1;

import java.net.*;
import java.io.*;
import java.util.*;
import java.util.Calendar;
import java.util.Date;
import java.text.Format;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;
import java.nio.channels.FileChannel;

public class NioServer {

    public static void main(String[] args) {
        ServerSocket server = null;
        int nioPort = 1235;
        Socket client;
        // Default port number we are going to use
        int portNumber = 1234;
        if (args.length >= 1) {
            portNumber = Integer.parseInt(args[0]);
        }
        // Create Server side socket
        try {
            server = new ServerSocket(portNumber);
        } catch (IOException ie) {
            System.out.println("Cannot open socket." + ie);
            System.exit(1);
        }
        System.out.println("ServerSocket is created " + server);
        // Wait for the data from the client and reply
        while (true) {
            try {
                // Listens for a connection to be made to
                // this socket and accepts it. The method blocks until
                // a connection is made
                System.out.println("Waiting for connect request at " + server.getInetAddress().toString() + ", port " +
                        server.getLocalPort() + "...");
                client = server.accept();
                System.out.println("Connect request is accepted...");
                String clientHost = client.getInetAddress().getHostAddress();
                int clientPort = client.getPort();
                System.out.println("Client host = " + clientHost + " Client port = " + clientPort);
                // Read data from the client
                InputStream clientIn = client.getInputStream();
                OutputStream clientOut = client.getOutputStream();
                BufferedReader br = new BufferedReader(new
                        InputStreamReader(clientIn));
                PrintWriter pw = new PrintWriter(clientOut, true);
                String fileName = "/Users/xujia/Exercise/giraffeexercise/src/" + br.readLine();
                System.out.println("File Requested by Client = " + fileName);

                // Send response to the client
//                String fileName = msgFromClient.split(" ")[1];
                System.out.println("File name = " + fileName);
                File file = new File(fileName);
                if (!file.exists()) {
                    //file doesn't exit, return error
                    System.out.println("File doesn't exist");
                    pw.println("Error: File doesn't exit");
                    pw.flush();
                } else {
                    ServerSocketChannel serverChannel = ServerSocketChannel.open();
                    serverChannel.configureBlocking(false);
                    serverChannel.socket().bind(new InetSocketAddress(nioPort));    // .socket() returns ServerSocket
                    System.out.println("Obtained server socket channel...");
                    Selector selector = Selector.open();
                    System.out.println("Selector open...");
                    //register server channel with the selector
                    serverChannel.register(selector, serverChannel.validOps());
                    // validOps will return SelectionKey.OP_ACCEPT for server socket channel
                    System.out.println("Server channel registered ....");
                    Iterator itr = null;

                    while (true) {
                        selector.select();
                        System.out.println("Channel selection made ....");

                        itr = selector.selectedKeys().iterator();
                        while (itr.hasNext()) {
                            SelectionKey selectedKey = (SelectionKey) itr.next();
                            System.out.println("Got Selection key...");
                            itr.remove();

                            //process the selected key
                            processSelectedKey(serverChannel, selector, selectedKey, fileName);
                            break;
                        }
                        //break;
                    }
                }
                client.close();
            } catch (IOException ie) {
            }
        }
    }

    public static void processSelectedKey(ServerSocketChannel server, Selector selector, SelectionKey selectedKey, String fileName) throws IOException {
        SocketChannel sChannel = null;
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        //System.out.println("IN");
        if (selectedKey.isValid()) {
            if (selectedKey.isAcceptable()) {
                sChannel = server.accept();
                sChannel.configureBlocking(false);
                sChannel.register(selector, SelectionKey.OP_WRITE);
            }
            if (selectedKey.isReadable()) {
                System.out.println("Seleceted key = readable...");
                sChannel = (SocketChannel) selectedKey.channel();
                buffer.clear();
                int nbrBytesRead = sChannel.read(buffer);
                while (nbrBytesRead != -1) {
                    buffer.flip();
                    String msgFromClient = new String(buffer.array());
                    System.out.println(msgFromClient);
                    buffer.clear();
                    nbrBytesRead = sChannel.read(buffer);
                }
                sChannel.close();
            } else if (selectedKey.isWritable()) {
                System.out.println("Seleceted key = writable ....");
                sChannel = (SocketChannel) selectedKey.channel();
                FileInputStream fis = new FileInputStream(new File(fileName));
                //System.out.println(fis.available());
                FileChannel fc = fis.getChannel();
                //buffer.clear();
                int nbrBytesReadFromFile = 0;
                long bytesSent = 0;
                int count = 0;
                while (nbrBytesReadFromFile != -1) {
                    buffer.clear();
                    nbrBytesReadFromFile = fc.read(buffer);
                    //System.out.println(nbrBytesReadFromFile + ":" + count);
                    //count++;
                    bytesSent += nbrBytesReadFromFile;
                    if (nbrBytesReadFromFile == -1) {
                        break;
                    }
                    buffer.flip();
                    sChannel.write(buffer);
                }
                fc.close();
                System.out.println(bytesSent);
                sChannel.close();
            }

        } else {
            System.out.println("Invalid key!");
        }
        //System.out.println("OUT");
    }

}