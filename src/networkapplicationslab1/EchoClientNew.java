package networkapplicationslab1;

import java.io.*;
import java.net.*;

public class EchoClientNew {

    private static final int PORT = 1234;

    // The echo protocol: TCP, using port 7, clients send data to echo server and server sends data base
    // TCP
    public static void main(String[] args) {
        String host = "localhost";
        PrintWriter out = null;
        BufferedReader in = null;
        BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Input commands:");
            String line;
            while ((line = stdIn.readLine()) != null) {
                if (line.equals("quit")) {
                    System.out.println("Program quit.");
                    break;
                }
                if (line.startsWith("DLOAD")) {
                    Socket s = new Socket(host, PORT);
                    in = new BufferedReader(new InputStreamReader(s.getInputStream()));
                    out = new PrintWriter(s.getOutputStream(), true);
                    System.out.println("Connected to echo server " + s.getInetAddress().getHostAddress()
                            + " on port " + s.getPort());
                    out.println(line);
                    System.out.println("Sent to server: " + line);
                    String fileName = line.substring(6);
                    saveFile(fileName, s);
                } else {
                    Socket s = new Socket(host, PORT);
                    in = new BufferedReader(new InputStreamReader(s.getInputStream()));
                    out = new PrintWriter(s.getOutputStream(), true);
                    System.out.println("Connected to echo server " + s.getInetAddress().getHostAddress()
                            + " on port " + s.getPort());
                    out.println(line);
                    System.out.println("Sent to server: " + line);
                    System.out.println("Receiverd from server: " + in.readLine());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // UDP
    /*public static void main(String[] args) {

        byte[] sentBuffer;
        byte[] receivedBuffer;
        String host = "localhost";
        BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
        try {
            DatagramSocket clientSocket = new DatagramSocket();
            InetAddress inetAddress = InetAddress.getByName(host);
            System.out.println("Send commands to server:");

            String line;
            while ((line = stdIn.readLine()) != null) {
                if (line.equals("quit")) {
                    System.out.println("Program quit.");
                    break;
                }
                sentBuffer = line.getBytes();
                DatagramPacket sentPacket = new DatagramPacket(sentBuffer, sentBuffer.length, inetAddress, PORT);
                clientSocket.send(sentPacket);
                System.out.println("Packet sent to server at " + sentPacket.getAddress() + ": " + line);
                System.out.println("Wating for server reply ...");
                receivedBuffer = new byte[1024];
                DatagramPacket receivedPacket = new DatagramPacket(receivedBuffer, receivedBuffer.length);
                clientSocket.receive(receivedPacket);
                byte[] data = new byte[receivedPacket.getLength()];
                System.arraycopy(receivedPacket.getData(), receivedPacket.getOffset(), data, 0, receivedPacket.getLength());
                String msgFromServer = new String(data);
                System.out.println("Packet received from server at " + receivedPacket.getAddress());
                System.out.println("Receiverd from server: " + msgFromServer);
            }
        }

        catch (IOException e) {
            e.printStackTrace();
        }

    }*/

    private static void saveFile(String fileName, Socket clientSocket) {
        String downloadDirectory = "/Users/xujia/Downloads/";
        int bytesRead;
        int current = 0;
        BufferedOutputStream bos = null;
        File fileToDownload = new File(downloadDirectory + fileName);
        if (fileToDownload.exists()) {
            System.out.println("File already exists!");
            return;
        }
        try {
            fileToDownload.createNewFile();
            byte[] fileArray = new byte[10000];
            InputStream is = null;
            try {
                is = clientSocket.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            bos = new BufferedOutputStream(new FileOutputStream(fileToDownload));
            bytesRead = is.read(fileArray, 0, fileArray.length);
            current = bytesRead;
            do {
                bytesRead = is.read(fileArray, current, (fileArray.length - current));
                if (bytesRead >= 0) {
                    current += bytesRead;
                }
            } while (bytesRead > -1);

            bos.write(fileArray, 0, current);
            bos.flush();
            System.out.println("File " + fileName
                    + " downloaded (" + current + " bytes read)");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (clientSocket != null) {
                try {
                    clientSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}

