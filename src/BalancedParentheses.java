/**
 * Created by Xujia Zhou on 1/28/18.
 */
public class BalancedParentheses {

    private int numberOfPairs;

    public BalancedParentheses(int numberOfPairs) {
        this.numberOfPairs = numberOfPairs;
    }

    public void printParentheses(int numberOfPairs) {
        printParentheses(new StringBuilder(), numberOfPairs, numberOfPairs);
    }

    private void printParentheses2(StringBuilder currentString, int numLeft, int numRight) {
        if (numLeft == 0 && numRight == 0) {
            System.out.println(currentString);
            return;
        }

        int originalLength = currentString.length();
        if (numLeft > 0) {
            currentString.append('(');
            printParentheses2(currentString, numLeft-1, numRight);
            currentString.delete(originalLength, currentString.length());
        }

        if (numLeft < numRight) {
            currentString.append(')');
            printParentheses2(currentString, numLeft, numRight-1);
            currentString.delete(originalLength, currentString.length());
        }
    }

    // numLeft stands for the unused (
    // numRight stands for the unused )
    private void printParentheses(StringBuilder currentString, int numLeft, int numRight) {
        // ending condition: when no ( is left, we can only add )
        if (numLeft == 0) {
            for (int i=1; i<=numRight; i++) {
                currentString.append(")");
            }
            // print the current solution
            System.out.println(currentString);
        } else {
            // it's very important to keep track of the current string
            // every time we enter the sub problem we need to keep track of the current status of currentString
            int currentLength = currentString.length();
            // suppose the current string is ******, the next parenthesis can either be a ( or a )
            // case (: we have numLeft number of ('s, so we can choose to print from 1 to numLeft consecutive ('s
            for (int i=1; i<=numLeft; i++) {
                // a new variation
                currentString.delete(currentLength, currentString.length());
                for (int j=1; j<=i; j++) {
                    currentString.append("(");
                }
                currentString.append(")");
                printParentheses(currentString, numLeft - i, numRight - 1);
            }
            // case ): we can also add ), depending on how many ) are more than ( at the current moment
            for (int i=1; i<=numRight - numLeft; i++) {
                currentString.delete(currentLength, currentString.length());
                for (int j=1; j<=i; j++) {
                    currentString.append(")");
                }
                currentString.append("(");
                printParentheses(currentString, numLeft - 1, numRight - i);
            }
        }
    }

    public static void main(String[] args) {
        BalancedParentheses bp = new BalancedParentheses(4);
        bp.printParentheses(bp.numberOfPairs);
    }

}
