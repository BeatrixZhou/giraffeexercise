package CoinProblems;

/*
/ author: Xujia Zhou
/ Given a set of coins with different values, what's the minimum number of coins needed to make a certain amount S?
*/
public class CoinMaster {

    private static int[] coins = {1, 2, 6, 11};
    private static int sum = 210;

    public static void main(String args[]) {
        masterCoin(sum, coins);
    }

    private static void masterCoin(int sum, int[] coins) {
        int[] min = new int[sum+1];
        int[] lastCoin = new int[sum+1]; // this is only for printing the optimal pile

        /* The solution is to use dynamic programming. The array min[i] represents all the minimum number of coins needed for
        * the sum i. For each new i, there are in total N (number of coins) solutions, because each time you could only add
        * one more coin, therefore it is equal to solving the sub-problem what's the minimum number of coins needed for sum
        * i - coins[j]. min[i] would be updated if a newer solution (e.g. using coins[3] instead of coins[2]) gives a smaller
        * number of coins. */
        min[0] = 0;
        for (int i=1; i<sum+1; i++) {
            min[i] = Integer.MAX_VALUE;
        }
        for (int i=1; i<sum+1; i++) {
            for (int j=0; j<coins.length; j++) {
                int tempValue = i - coins[j];
                if (tempValue >= 0 && min[i] > 1 + min[tempValue]) {
                    min[i] = 1 + min[tempValue];
                    lastCoin[i] = j;
                    System.out.printf("Current sum: %d, sub-coin pile: %d + %d%n", i, tempValue, coins[j]);
                    System.out.printf("New min[%d]: %d%n%n", i, min[i]);
                }
            }
        }
        System.out.println("JiajiaGiraffe presents you the optimal coin pile: ");
        int tempSum = sum;
        while (tempSum > 0) {
            System.out.print(coins[lastCoin[tempSum]]);
            tempSum -= coins[lastCoin[tempSum]];
            if (tempSum > 0) {
                System.out.print(", ");
            }
        }
    }
}

