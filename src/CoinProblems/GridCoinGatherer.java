package CoinProblems;

/**
 * @author JiajiaMouse
 * Given a coin grid n*n, and a greedy cute little mouse robot who gathers the coins in the grid
 * The mouse robot starts from the bottom edge and
 * For each step, she can only move diagonally upward or straight upward
 * How will she make her fortune ?
 */

public class GridCoinGatherer {

    private int[][] coinGrid;

    // this class has no need to reference any CoinProblems.GridCoinGatherer instance
    private static class Cell {
        int coins;
        int row;
        int column;
        int maxGain;
        Cell lastCell;

        Cell(int coins, int row, int column) {
            this.coins = coins;
            this.row = row;
            this.column = column;
        }
    }

    public GridCoinGatherer(int[][] coinGrid) {
        this.coinGrid = coinGrid;
    }

    private void gatherCoin() {

        int n = coinGrid.length;
        Cell[][] cells = new Cell[n][n];

        for (int i=0; i<n; i++) {
            for (int j=0; j<n; j++) {
                cells[i][j] = new Cell(coinGrid[i][j], i, j);
            }
        }

        // initialize maxGain of bottom-row cells, lastCell is null
        for (int j=0; j<n; j++) {
            cells[n-1][j].maxGain = cells[n-1][j].coins;
        }

        // move up and get the maxGain and lastStep of the remaining cells
        for (int i=n-2; i>=0; i--) {
            for (int j=0; j<n; j++) {
                cells[i][j].lastCell = cells[i+1][j];
                if (j < n-1 && cells[i+1][j+1].maxGain > cells[i][j].lastCell.maxGain) {
                    cells[i][j].lastCell = cells[i+1][j+1];
                }
                if (j > 0 && cells[i+1][j-1].maxGain > cells[i][j].lastCell.maxGain) {
                    cells[i][j].lastCell = cells[i+1][j-1];
                }
                cells[i][j].maxGain = cells[i][j].lastCell.maxGain + cells[i][j].coins;
            }
        }

        // find the max accumulative coin value in the top row and print the steps
        Cell maxCell = cells[0][0];
        for (int j=1; j<n; j++) {
            if (cells[0][j].maxGain > maxCell.maxGain) {
                maxCell = cells[0][j];
            }
        }
        System.out.println("JiajiaMouse has gathered " + maxCell.maxGain + " coins!");
        System.out.println("Her steps:");
        while (maxCell != null) {
            System.out.println(String.format("(%d,%d): %d", maxCell.row, maxCell.column, maxCell.coins));
            maxCell = maxCell.lastCell;
        }
    }

    public static void main(String[] args) {
         int[][] coinGrid = {{2, 4, 5, 6, 8},
                             {3, 1, 5, 7, 3},
                             {4, 5, 2, 2, 1},
                             {2, 3, 3, 1, 7},
                             {7, 3, 9, 1, 7}};
         GridCoinGatherer jiajiaMouse = new GridCoinGatherer(coinGrid);
         jiajiaMouse.gatherCoin();
    }

}
