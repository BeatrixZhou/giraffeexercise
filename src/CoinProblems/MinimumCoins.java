package CoinProblems;

/**
 * Given a value V, if we want to make change for V cents,
 * and we have infinite supply of each of C = { C1, C2, .. , Cm} valued coins,
 * what is the minimum number of coins to make the change?
 */

public class MinimumCoins {

    private int[] coins;

    public MinimumCoins(int[] coins) {
        this.coins = coins;
    }

    public int getMinimumCoins(int value) {
        int[] trackingTable = new int[value + 1];
        return getMinimumCoinsRecursive(value, trackingTable);
    }

    private int getMinimumCoinsRecursive(int value, int[] trackingTable) {

        // Base case
        if (value == 0) {
            return 0;
        }

        int result = Integer.MAX_VALUE;

        for (int i = 0; i < coins.length; i++) {
            if (value >= coins[i]) {    // don't forget about this condition and it has to be >= not > otherwise it will never reach the base case
                int subResult = Integer.MAX_VALUE;
                if (trackingTable[value - coins[i]] != 0) {
                    subResult = trackingTable[value - coins[i]];
                } else {
                    subResult = getMinimumCoinsRecursive(value - coins[i], trackingTable);
                    // trackingTable[value - coins[i]] = subResult;
                }
                if (subResult + 1 < result) {
                    result = subResult + 1;
                    trackingTable[value] = result;    // the tracking table can be updated either here or above
                }
            }
        }
        return result;
    }

    private int getMinimumCoinsDynamic(int value) {
        int[] trackingTable = new int[value + 1];
        for (int i=0; i<coins.length; i++) {
            trackingTable[coins[i]] = 1;
        }
        for (int i=1; i<value; i++) {
            for (int j=0; j<coins.length; j++) {
                if (i + coins[j] <= value) {
                    if (trackingTable[i + coins[j]] == 0) {
                        trackingTable[i + coins[j]] = trackingTable[i] + 1;
                    } else if (trackingTable[i + coins[j]] > trackingTable[i] + 1) {
                        trackingTable[i + coins[j]] = trackingTable[i] + 1;
                    }
                }
            }
        }
        return trackingTable[value];
    }

    public static void main(String[] args) {
        int[] coins = {1, 2, 3, 6, 11};
        MinimumCoins minimumCoins = new MinimumCoins(coins);
        System.out.println("Minimum coins using recursion: " + minimumCoins.getMinimumCoins(1200));
        System.out.println("Minimum coins using dynamic programming: " + minimumCoins.getMinimumCoinsDynamic(1200));
    }

}
