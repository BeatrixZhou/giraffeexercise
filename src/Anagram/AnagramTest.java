package Anagram;

import Anagram.Anagram;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class AnagramTest {

    @org.junit.Test
    public void testIsAnagramJava7ForShuffledString() {
        String string = generateRandomString();
        System.out.println(string);
        String shuffledString = shuffle(string);
        System.out.println(shuffledString);
        assertTrue(Anagram.isAnagramJava7(string, shuffledString));
    }

    @org.junit.Test
    public void testIsAnagramJava7ForTamperedString() {
        String string = generateRandomString();
        System.out.println(string);
        String tamperedString = tamper(string);
        System.out.println(tamperedString);
        assertFalse(Anagram.isAnagramJava7(string, tamperedString));
    }

    @org.junit.Test
    public void testIsAnagramJava8ForShuffledString() {
        String string = generateRandomString();
        System.out.println(string);
        String shuffledString = shuffle(string);
        System.out.println(shuffledString);
        assertTrue(Anagram.isAnagramJava8(string, shuffledString));
    }

    @org.junit.Test
    public void testIsAnagramJava8ForTamperedString() {
        String string = generateRandomString();
        System.out.println(string);
        String tamperedString = tamper(string);
        System.out.println(tamperedString);
        assertFalse(Anagram.isAnagramJava8(string, tamperedString));
    }

    private static String generateRandomString() {
        int stringLength = new Random().nextInt(10) + 5;
        StringBuilder builder = new StringBuilder();
        for (int i=0; i<stringLength; i++) {
            builder.append((char) ('a' + new Random().nextInt(26)));
        }
        return builder.toString();
    }

    private static String shuffle(String string) {
        // convert string to list
        List<Character> charList = string.chars().mapToObj(character -> (char) character).collect(Collectors.toList());
        Collections.shuffle(charList);
        StringBuilder builder = new StringBuilder();
        charList.stream().forEach((character) -> builder.append(character));
        return builder.toString();
    }

    private static String tamper(String string) {
        char[] charArray = string.toCharArray();
        int tamperChoice = new Random().nextInt(3);
        int tamperPosition = new Random().nextInt(string.length());
        if (tamperChoice==0) {
            char newChar;
            do {
                newChar = (char) ('a' + new Random().nextInt(26));
            } while (newChar==charArray[tamperPosition]);
            charArray[tamperPosition] = newChar;
            return charArray.toString();
        } else if (tamperChoice==1) {
            for (int i=0; i<5; i++) {
                string = string + (char) ('a' + new Random().nextInt(26));
            }
            return string;
        } else {
            char[] newCharArray = new char[string.length()-5];
            Arrays.copyOf(charArray, string.length()-5);
            return newCharArray.toString();
        }
    }

}