package Anagram;

import java.util.*;

/**
 * @author Xujia Zhou
 * given two strings, tell if they are anagram or not
 * anagram means they are composed of exactly the same letters
 */
public class Anagram {

    public static boolean isAnagramJava7(String string1, String string2) {
        HashMap<Character, Integer> map = new HashMap<>();
        // construct the map from string1
        for (int i=0; i<string1.length(); i++) {
            if (map.containsKey(string1.charAt(i))) {
                int currentTimesOfOccurence = map.get(string1.charAt(i));
                currentTimesOfOccurence++;
                map.put(string1.charAt(i), currentTimesOfOccurence);
            } else {
                map.put(string1.charAt(i), 1);
            }
        }
        // compare string2 with string1 using the map
        for (int i=0; i<string2.length(); i++) {
            if (!map.containsKey(string2.charAt(i))) {
                System.out.println("This character is not found: " + string2.charAt(i));
                return false;
            } else if (map.get(string2.charAt(i))==1) {
                map.remove(string2.charAt(i));
            } else {
                int currentTimesOfOccurence = map.get(string2.charAt(i));
                currentTimesOfOccurence--;
                map.put(string2.charAt(i), currentTimesOfOccurence);
            }
        }
        if (map.isEmpty()) {
            return true;
        } else {
            System.out.println("No match for the following characters in string1: " + map.keySet());
            return false;
        }
    }

    public static boolean isAnagramJava8(String string1, String string2) {
        HashMap<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < string1.length(); i++) {
            map.computeIfPresent(string1.charAt(i), (character, timesOfOccurence) -> ++timesOfOccurence);
            map.putIfAbsent(string1.charAt(i), 1);
        }
        for (int i = 0; i < string2.length(); i++) {
            if (map.computeIfPresent(string2.charAt(i), (character, timesOfOccurence) -> {
                if (timesOfOccurence==0) {
                    return null;
                } else {
                    return timesOfOccurence-1;
                }
            }) == null) {
                System.out.println("This character is not found: " + string2.charAt(i));
                return false;
            };
        }
        return map.values().stream().allMatch((value) -> value==0);
    }

    public static void main(String args[]) {
        System.out.println(isAnagramJava7("miao", "mi"));
        System.out.println(isAnagramJava8("miao", "miaow"));
    }

}