package Anagram;

import java.util.*;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GiraffeAnagram {

    public static void main(String[] args) {
        List<String> giraffeStrings = Arrays.asList("gariffe", "fefragi", "giraffe", "girapolarbearffe");
        System.out.printf("The strings are %sanagrams.%n", areAnagrams(giraffeStrings) ? "" : "not ");
        System.out.printf("The strings are %sanagrams.%n", areAnagramsWithoutMap(giraffeStrings) ? "" : "not ");
        System.out.printf("The strings are %sanagrams.%n", areAnagramsUsingDistinct(giraffeStrings) ? "" : "not ");
    }

    // with the map we can count how many 'bad' strings there are
    private static boolean areAnagrams(List<String> stringList) {
        Map<String, List<String>> anagrams = stringList.stream()
                .collect(Collectors.groupingBy(sortedLetters));
        return anagrams.size()==1;
    }

    private static boolean areAnagramsWithoutMap(List<String> stringList) {
        String firstElementSorted = sortedLetters.apply(stringList.get(0));
        Predicate<String> matchFirst = string -> sortedLetters.apply(string).equals(firstElementSorted);
        return stringList.stream().allMatch(matchFirst);
    }

    private static boolean areAnagramsUsingDistinct(List<String> stringList) {
        Long count = stringList.stream()
                .map(sortedLetters)
                .distinct()
                .count();
        return count == 1;
    }

    static UnaryOperator<String> sortedLetters = s -> Stream.of(s.split(""))
            .sorted()
            .collect(Collectors.joining());

}

