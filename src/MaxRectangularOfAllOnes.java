/**
 * @author Dabian, Xujia
 * Given a rectangle grid M*N, each square filled with either 0 or 1,
 * find the largest square in the grid with all 1s,
 * and further on, find the largest rectangle with all 1s
 */
public class MaxRectangularOfAllOnes {
    static int[][] matrix = {{0,0,1,1,0},
                             {1,1,1,1,0},
                             {1,1,1,1,0},
                             {0,0,1,1,0},
                             {0,0,1,1,0}};

    public static void main(String[] args) {
        findMaxSquare(matrix);
        findMaxRectangle(matrix);
    }

    public static void findMaxSquare(int[][] matrix) {
        int rows = matrix.length;
        int columns = matrix[0].length;
        // the max edge length of the all-one square whose bottom-right point is at i,j
        int[][] maxEdgeLength = new int[rows][columns];

        // record the biggest square
        int maxI = -1;
        int maxJ = -1;
        int maxEdge = 0;

        // fill first row
        for (int i=0; i<columns; i++) {
            maxEdgeLength[0][i] = matrix[0][i];
            if (maxEdgeLength[0][i] > maxEdge) {
                maxEdge = maxEdgeLength[0][i];
                maxI = 0;
                maxJ = i;
            }
        }

        // fill first column
        for (int i=0; i<rows; i++) {
            maxEdgeLength[i][0] = matrix[i][0];
            if (maxEdgeLength[i][0] > maxEdge) {
                maxEdge = maxEdgeLength[i][0];
                maxI = i;
                maxJ = 0;
            }
        }

        // fill the rest rows and columns
        for (int i=1; i<rows; i++) {
            for (int j=1; j<columns; j++) {
                if (matrix[i][j] == 0) {
                    maxEdgeLength[i][j] = 0;
                } else {
                    maxEdgeLength[i][j] = Math.min( Math.min(maxEdgeLength[i][j-1], maxEdgeLength[i-1][j]),
                                                    maxEdgeLength[i-1][j-1]) + 1;
                }
                if (maxEdgeLength[i][j] > maxEdge) {
                    maxEdge = maxEdgeLength[i][j];
                    maxI = i;
                    maxJ = j;
                }
            }
        }

        System.out.println(String.format("Bottom right point for biggest square: %d, %d", maxI, maxJ));
        System.out.println(String.format("Edge length: %d", maxEdge));
    }

    public static void findMaxRectangle(int[][] matrix) {
        int rows = matrix.length;
        int columns = matrix[0].length;
        // the max edge height of the all-one square whose bottom-right point is at i,j
        int[][] maxEdgeLengthHeight = new int[rows][columns];
        // the max edge width of the all-one square whose bottom-right point is at i,j
        int[][] maxEdgeLengthWidth = new int[rows][columns];

        // record the biggest rectangle
        int maxI = -1;
        int maxJ = -1;
        int maxEdgeHeight = 0;
        int maxEdgeWidth = 0;

        // fill first row
        for (int i=0; i<columns; i++) {
            if (matrix[0][i] == 0) {
                maxEdgeLengthHeight[0][i] = 0;
                maxEdgeLengthWidth[0][i] = 0;
            } else {
                maxEdgeLengthHeight[0][i] = 1;
                if (i==0) {
                    maxEdgeLengthWidth[0][i] = 1;
                } else {
                    maxEdgeLengthWidth[0][i] = Math.max(maxEdgeLengthWidth[0][i-1] + 1, matrix[0][i]);
                }
                if (maxEdgeLengthHeight[0][i]*maxEdgeLengthWidth[0][i] > maxEdgeHeight*maxEdgeWidth) {
                    maxEdgeHeight = 1;
                    maxEdgeWidth = maxEdgeLengthWidth[0][i];
                    maxI = 0;
                    maxJ = i;
                }
            }
        }

        // fill first column
        for (int i=0; i<rows; i++) {
            if (matrix[i][0] == 0) {
                maxEdgeLengthHeight[i][0] = 0;
                maxEdgeLengthWidth[i][0] = 0;
            } else {
                maxEdgeLengthWidth[i][0] = 1;
                if (i==0) {
                    maxEdgeLengthHeight[i][0] = 1;
                } else {
                    maxEdgeLengthHeight[i][0] = Math.max(maxEdgeLengthHeight[i-1][0] + 1, matrix[i][0]);
                }
                if (maxEdgeLengthHeight[i][0]*maxEdgeLengthWidth[i][0] > maxEdgeHeight*maxEdgeWidth) {
                    maxEdgeHeight = maxEdgeLengthHeight[i][0];
                    maxEdgeWidth = 1;
                    maxI = i;
                    maxJ = 0;
                }
            }
        }

        // fill the rest rows and columns
        for (int i=1; i<rows; i++) {
            for (int j=1; j<columns; j++) {
                if (matrix[i][j] == 0) {
                    maxEdgeLengthHeight[i][j] = 0;
                    maxEdgeLengthWidth[i][j] = 0;
                } else {
                    int areaLeft = (maxEdgeLengthWidth[i][j-1] + 1) *
                                   Math.min(maxEdgeLengthHeight[i][j-1], maxEdgeLengthHeight[i-1][j] + 1);
                    int areaUp = (maxEdgeLengthHeight[i-1][j] + 1) *
                                 Math.min(maxEdgeLengthWidth[i][j-1] + 1, maxEdgeLengthWidth[i-1][j]);
                    int areaDiagnal = Math.min(maxEdgeLengthHeight[i-1][j-1] + 1, maxEdgeLengthHeight[i-1][j] + 1) *
                                      Math.min(maxEdgeLengthWidth[i-1][j-1] + 1, maxEdgeLengthWidth[i][j-1] + 1);
                    int maxArea = Math.max(areaDiagnal, Math.max(areaLeft, areaUp));
                    if (maxArea == areaLeft) {
                        maxEdgeLengthHeight[i][j] = Math.min(maxEdgeLengthHeight[i][j-1], maxEdgeLengthHeight[i-1][j] + 1);
                        maxEdgeLengthWidth[i][j] = maxEdgeLengthWidth[i][j-1] + 1;
                    } else if (maxArea == areaUp) {
                        maxEdgeLengthHeight[i][j] = maxEdgeLengthHeight[i-1][j] + 1;
                        maxEdgeLengthWidth[i][j] = Math.min(maxEdgeLengthWidth[i-1][j], maxEdgeLengthWidth[i][j-1] + 1);
                    } else {
                        maxEdgeLengthHeight[i][j] = Math.min(maxEdgeLengthHeight[i-1][j-1] + 1, maxEdgeLengthHeight[i-1][j] + 1);
                        maxEdgeLengthWidth[i][j] = Math.min(maxEdgeLengthWidth[i-1][j-1] + 1, maxEdgeLengthWidth[i][j-1] + 1);
                    }
                }

                System.out.println(String.format("i=%d, j=%d, h=%d, w=%d", i, j, maxEdgeLengthHeight[i][j], maxEdgeLengthWidth[i][j]));

                if (maxEdgeLengthHeight[i][j]*maxEdgeLengthWidth[i][j] > maxEdgeHeight*maxEdgeWidth) {
                    maxEdgeHeight = maxEdgeLengthHeight[i][j];
                    maxEdgeWidth = maxEdgeLengthWidth[i][j];
                    maxI = i;
                    maxJ = j;
                }
            }
        }

        System.out.println(String.format("Bottom right point for biggest rectangle: [%d, %d]", maxI, maxJ));
        System.out.println(String.format("Edge height: %d", maxEdgeHeight));
        System.out.println(String.format("Edge width: %d", maxEdgeWidth));
    }

}
