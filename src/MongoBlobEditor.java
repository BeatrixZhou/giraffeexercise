/*
/ this one is incompletes
 */

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MongoBlobEditor {

    List<String> lines = new ArrayList<String>();
    String line = null;

    private void generateJsonObject() {
        try {
            File fileToRead = new File("/Users/xujia/Desktop/meetio_stats_production_data.json");
            File fileToWrite = new File("/Users/xujia/Desktop/test.json");
            FileReader fr = new FileReader(fileToRead);
            FileWriter fw = new FileWriter(fileToWrite);
            BufferedReader br = new BufferedReader(fr);
            BufferedWriter bw = new BufferedWriter(fw);

            // [0-9]{10} // digit 0-9, exactly 10 digits

            while ((line = br.readLine()) != null) {

                StringBuffer sb = new StringBuffer(line);
                sb.append(",\n");
                String newLine = sb.toString();
                bw.write(newLine);
            }
            br.close();
            bw.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
        }
    }

    private void removePassword() {

        try {
            File fileToRead = new File("/Users/xujia/meetio/data/wp.csv");
            File fileToWrite = new File("/Users/xujia/meetio/data/wp1.csv");
            FileReader fr = new FileReader(fileToRead);
            FileWriter fw = new FileWriter(fileToWrite);
            BufferedReader br = new BufferedReader(fr);
            BufferedWriter bw = new BufferedWriter(fw);

            // [0-9]{10} // digit 0-9, exactly 10 digits

            // total occurrence ews = 1586;
            // ews_room : 1016
            int counter = 0;

            Pattern pattern = Pattern.compile("\\\\\"ews_password\\\\\":\\\\\".*,\\\\\"calendar_");

            while ((line = br.readLine()) != null) {
                Matcher matcher = pattern.matcher(line);
                StringBuffer sb = new StringBuffer(line.length());
                while (matcher.find()) {
                    matcher.appendReplacement(sb, "\\\\\"calendar_");
                    counter++;
                }
                matcher.appendTail(sb);
                String newLine = sb.toString();
                System.out.println(newLine);
                bw.write(newLine);
            }
            System.out.println(counter);
            br.close();
            bw.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
        }
    }

    public static void main(String args[]) {
        MongoBlobEditor editor = new MongoBlobEditor();
        // editor.generateJsonObject();
        editor.removePassword();
    }

}
