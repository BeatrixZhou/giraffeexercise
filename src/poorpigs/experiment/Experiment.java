package poorpigs.experiment;

import java.util.BitSet;
import java.util.Random;

/**
 * Run a cruel experiment to find out which bucket is poisonous.
 */
public class Experiment {
    public static final int TOTAL_NUMBER_OF_BUCKETS = 10000;
    private final int poisonousBucket;
    private BitSet livingPigs;

    /**
     * Create an experiment setup
     *
     * @param numberOfPigs   number of poor pigs to use
     */
    public Experiment(int numberOfPigs) {
        poisonousBucket = new Random().nextInt(TOTAL_NUMBER_OF_BUCKETS);
        livingPigs = new BitSet(numberOfPigs);
        livingPigs.set(0, numberOfPigs);
    }

    /**
     * Feed the pigs and advance 15 min
     *
     * @param bucketsForEachPig only buckets with True status will be fed to the pigs
     */
    public void feedPigs(BitSet[] bucketsForEachPig) {
        for(int pig = 0; pig < bucketsForEachPig.length; pig++) {
            if (livingPigs.get(pig) && bucketsForEachPig[pig].get(poisonousBucket)) {
                killPig(pig);
            }
        }
    }

    private void killPig(int pig) {
        livingPigs.clear(pig);
    }

    /**
     * Get living pigs
     */
    public BitSet getLivingPigs() {
        return (BitSet) this.livingPigs.clone();
    }

    /**
     * Report poisonous bucket
     */
    public void reportPoisonousBucket(int reportedPoisonousBucket) {
        System.out.println("Poisonous bucket: " + this.poisonousBucket);
        if (this.poisonousBucket == reportedPoisonousBucket) {
            System.out.println("The pigs have found the poisonous bucket!");
        } else {
            System.out.println("The pigs haven't found the poisonous bucket!");
        }
    }
}
