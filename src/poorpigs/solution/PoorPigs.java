package poorpigs.solution;

import poorpigs.experiment.Experiment;

import java.util.Arrays;
import java.util.BitSet;

/**
 * There are a given number of buckets, one and only one of them contains poison, the rest are filled with water.
 * They all look the same. If a pig drinks that poison it will die within 15 minutes.
 * What is the minimum amount of pigs you need to figure out which bucket contains the poison within one hour.
 */
public class PoorPigs {
    public static void main(String[] args) {
        int numberOfPigs = (int) Math.ceil(Math.log((double) Experiment.TOTAL_NUMBER_OF_BUCKETS)/ Math.log(4));
        System.out.println(numberOfPigs + " brave pigs are on the mission of finding the poisonous bucket:");
        Experiment experiment = new Experiment(numberOfPigs);

        int rounds = 60/15;
        char[] deathRecord = new char[numberOfPigs];
        Arrays.fill(deathRecord, 'p');

        for (int round=0; round<rounds; round++) {
            BitSet[] bucketsForEachPig = new BitSet[numberOfPigs];
            BitSet livingPigs = experiment.getLivingPigs();
            for (int pig=0; pig<numberOfPigs; pig++) {
                if (livingPigs.get(pig)) {
                    BitSet bucketsForThisPig = new BitSet(Experiment.TOTAL_NUMBER_OF_BUCKETS);
                    for (int bucketNumber = 0; bucketNumber < Experiment.TOTAL_NUMBER_OF_BUCKETS; bucketNumber++) {
                        int digitOnPig = getNthDigit(bucketNumber, pig, rounds);
                        if (digitOnPig == round) {
                            bucketsForThisPig.set(bucketNumber);
                        }
                    }
                    bucketsForEachPig[pig] = bucketsForThisPig;
                }
            }
            experiment.feedPigs(bucketsForEachPig);

            BitSet stillLivingPigs = experiment.getLivingPigs();

            for (int i=0; i<stillLivingPigs.size(); i++) {
                if (livingPigs.get(i) && !stillLivingPigs.get(i) ) {
                    deathRecord[numberOfPigs - 1 - i] = Character.forDigit(round, rounds);
                }
            }

            System.out.println(String.valueOf(deathRecord));
        }

        int poisonousBucket = Integer.valueOf(String.valueOf(deathRecord), rounds);

        System.out.println(poisonousBucket);
        experiment.reportPoisonousBucket(poisonousBucket);
    }

    private static int getNthDigit(int decimal, int n, int base) {
        int digit = decimal % (int)Math.pow( (double)base, n + 1);
        return digit / (int)Math.pow( (double)base, n);
    }
}
