package LIS;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author Andreas Nilsson <andreas.nilsson@hkr.se>
 */
public class Node {
    private static List<Node> nodes = new ArrayList<>();                            // Used to keep track of ALL nodes

    private final List<Node> children = new ArrayList<>();                          // All nodes in the tree keep track of its children
    public final Node parent;                                                       // ... and its parent
    public final int value;                                                         // The nodes actual value
    public final int length;                                                        // Keeps track of the length of the list (including this node)

    public Node() {
        this(-1);                                                             // The root node gets a negative value so that all numbers will be greater
    }

    public Node(int value) {
        this(value, 0, null);
    }

    public Node(int value, int length, Node parent) {
        this.value = value;
        this.length = length;
        this.parent = parent;
    }

    public List<Node> getNodes() {
        return Collections.unmodifiableList(nodes);                                 // Always return a list that can not be modified
    }

    public void add(Node node) {
        Optional<Integer> smallestChild = children.stream()                         // Stream all the children
                .map(n -> n.value)                // Look at their value
                .sorted()                         // Sort the values
                .findFirst();                     // Take the smallest (first)

        Node updatedNode = new Node(node.value,                                     // Increase the length to reflect that the node has moved down in the tree
                node.length + 1,
                this);


        if (node.value > value && isLeaf()) {                                       // If the new value is greater than the current node
            nodes.add(updatedNode);                                                 // and the current node is a leaf, add the node as a child
            children.add(updatedNode);
        } else if (node.value > value                                               // If the new value is greater than the current node
                && smallestChild.isPresent()                                        // and this node has children, but all children's value are greater.
                && node.value < smallestChild.get()) {                              // Fork from this node and create a new branch (add a new child)
            nodes.add(updatedNode);
            children.add(updatedNode);
        } else {                                                                    // In all other cases
            for (Node child : children) {                                           // Send the node down to all children
                if (node.value > child.value) {                                     // ... but only if the value is greater than the child
                    child.add(updatedNode.clone());
                }
            }
        }
    }

    public boolean isLeaf() {
        return children.isEmpty();
    }

    public boolean isRoot() {
        return parent == null;
    }

    public Node clone() {
        return new Node(value, length, parent);
    }
}

