package LIS;

import java.lang.reflect.Array;
import java.util.ArrayList;

/*
/ author: Xujia Zhou
/ Given an array, return the longest increasing incontiguous subsequence
*/
public class LongestIncontiguousIncreasingSusequence {

    private static int[] inputArray = {9, 8, 7, 5, 1, 6, 8, 3, 8, 10, 1, 2, 5, 9, 3, 4, 7};

    public static void main(String args[]) {
        // printLIS(inputArray);
        ArrayList<Integer> LIS = getLIS(inputArray);
        for(int i=1; i<LIS.size(); i++){
            System.out.print(LIS.get(i) + " ");
        }
    }

    // this method prints out all LIS (first sequence found) at each position of ogirinal array
    private static void printLIS(int[] array) {
        // LIS[i] represents the LIS of the element at position [i] in the original array
        int[] LIS = new int[array.length];
        for (int i=0; i<array.length; i++) {
            LIS[i] = 0;
        }
        // represents the longest sequence at all positions but only the first solution found
        String[] LISString = new String[array.length];
        for (int i=0; i<array.length; i++) {
            LISString[i] = "";
        }

        System.out.println("LIS of " + array[0] + ": " + 1);
        System.out.println(array[0]);

        for (int i=1; i<array.length; i++) {
            int currentLongestNumber = 0;
            int currentPosition = 0;
            for (int j=i-1; j>=0; j--) {
                if (array[i] > array[j]) {
                    if (LIS[j] > currentLongestNumber) {
                        currentLongestNumber = LIS[j];
                        currentPosition = j;
                    }
                }
                LIS[i] = currentLongestNumber + 1;
                if (LISString[currentPosition].equals("")) {
                    LISString[i] = Integer.toString(array[i]);
                } else {
                    LISString[i] = LISString[currentPosition] + " " + Integer.toString(array[i]);
                }
            }
            System.out.println("LIS of " + array[i] + ": " + LIS[i]);
            System.out.println(LISString[i]);
        }
    }

    // the binary search method has O(nlogn) complexity which is better, but it only prints out the smallest possible sequence
    private static ArrayList<Integer> getLIS(int[] inputArray) {
        ArrayList<Integer> LIS = new ArrayList<>();
        // let the first element be 0 so that LIS.get(i) would be the ith smallest number in the LIS sequence
        LIS.add(0);
        for (int i=0; i<inputArray.length; i++) {
            int positionToRaplace = binarySearchFIS(LIS, inputArray[i]);
            if (positionToRaplace == -1) {
                continue;
            } else if (positionToRaplace == -2) {
                LIS.add(inputArray[i]);
            } else {
                LIS.set(positionToRaplace, inputArray[i]);
            }
        }
        return LIS;
    }

    // returns the position of the smallest number in exisitng LIS that is bigger than target
    private static int binarySearchFIS(ArrayList<Integer> LIS, int target) {
        int low = 0;
        int high = LIS.size() - 1;
        while (low <= high) {
            int mid = (low + high) / 2;
            if (LIS.get(mid) == target) {
                return -1;
            } else if (LIS.get(mid) < target && mid+1 < LIS.size() && LIS.get(mid+1) > target) {
                return mid+1;
            } else if (LIS.get(mid) < target) {
                low = mid + 1;
            } else {
                high = mid -1;
            }
        }
        // returns -2 means the new element is bigger than all the elements in the existing LIS
        return -2;
    }
}