package LIS;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author Andreas Nilsson <andreas.nilsson@hkr.se>
 */

public class LISPrint {

    public static void main(String[] args) {
        LISPrint myApp = new LISPrint();
        List<Node> nodes = myApp.createRandomList(10);
        Node root = new Node();                                                    // Root node for the tree

        nodes.stream().forEach(root::add);                                         // Generate the tree by letting all numbers propagate down the tree structure

        System.out.println(" --- --- The Numbers --- ---");
        System.out.println(nodes.stream()                                           // Stream all nodes
                .map(node -> String.valueOf(node.value))            // Pick the nodes values
                .collect(Collectors.joining(", ")));       // Combine into one string, separated by commas
        System.out.println(" ---------------------------");

        int maxLength = root.getNodes().stream()                                    // Stream all nodes
                .map(node -> node.length)                               // Look at the length of the nodes
                .max(Integer::compare).get();                           // Pick the greatest one

        root.getNodes().stream()                                                    // Stream all nodes
                .filter(node -> node.isLeaf())                                          // Keep only the leaf nodes
                .filter(node -> node.length == maxLength)                               // Filter out all but the nodes that has the same length as the longest max length
                .forEach(myApp::printLink);                                             // Print them
    }

    private List<Node> createRandomList(int length) {
        List<Node> nodes = IntStream.of(9, 8, 7, 4, 1, 5, 6, 2, 3)                  // Create an integer stream using the listed numbers
//        List<Node> nodes = IntStream.range(1, length)                               // Create an Integer stream from 1 to length
                .mapToObj(Node::new)                            // Create a new Node using the Integer
                .collect(Collectors.toList());                  // Return all nodes as a List

//        Collections.shuffle(nodes);                                                 // Shuffle the nodes
        return nodes;
    }

    private void printLink(Node leaf) {
        List<Node> nodes = new ArrayList<>();                                       // Used to hold the subgroup
        Node curr = leaf;                                                           // Start from the Leaf

        while (!curr.isRoot()) {                                                    // Traverse backwards till we reach the root node
            nodes.add(0, curr);                                               // Add to the front of the list to get the list in correct order
            curr = curr.parent;
        }

        System.out.println(nodes.stream()                                           // Take the now reversed nodes
                .map(node -> String.valueOf(node.value))            // Map the value into a String representation
                .collect(Collectors.joining(" -> ")));     // Concatenate into one string and add a -> between the numbers
    }
}

