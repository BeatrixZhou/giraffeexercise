package findxjapan;

/**
 * A detective class.
 * Given a known concert, find x-japan.
 */
public class XJapanFinder {

    public static void main(String[] args) {
        Concert concert = new Concert();
        XJapanFinder finder = new XJapanFinder();
        System.out.printf("X-Japan is: %d!!!", finder.findXjapan(concert));
    }

    /**
     * Finds out who is x-japan.
     */
    private int findXjapan(Concert concert) {
        int numberOfPeople = concert.getCrowdSize();
        int xjapanIndex = 0;    // Let first person in the crowd be temp x-japan
        int i = 1;    // Second person in the crowd

        // Loop through the crowd, ask two at a time
        while (i < numberOfPeople) {
            // Temp x-japan and i know each other, neither of them are x-japan
            if (concert.knows(xjapanIndex, i) && concert.knows(i, xjapanIndex)) {
                xjapanIndex = i + 1;
                i += 2;
            // Only temp x-japan knows i, maybe i is x-japan
            } else if (concert.knows(xjapanIndex, i)) {
                xjapanIndex = i;
                i++;
            // Only i knows temp x-japan, maybe temp x-japan is indeed x-japan
            } else if (concert.knows(i, xjapanIndex)) {
                i++;
            // Temp x-japan and i don't know each other, neither of them are x-japan
            } else {
                xjapanIndex = i + 1;
                i += 2;
            }
        }

        return xjapanIndex;
    }

}
