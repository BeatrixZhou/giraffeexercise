/**
 * @author JiajiaGiraffeFan
 * There are n people out of which one is x-japan and the rest are their fan.
 * x-japan doesn't know anyone else while everyone else knows x-japan.
 * Exposing a method which tells whehter A knows B, how can we fastest find x-japan?
 */

package findxjapan;

import java.util.Random;

public class Concert {

    private int numberOfPeople;
    private boolean[][] whoKnowsWho;
    private int xjapanIndex;

    public Concert() {
        numberOfPeople = new Random().nextInt(1000) + 1000;
        System.out.println("A big crowd of " + numberOfPeople + " people came to see X-Japan's concert!");
        whoKnowsWho = new boolean[numberOfPeople][numberOfPeople];
        generateRelationshipMatrix();
    }

    private void generateRelationshipMatrix() {
        Random random = new Random();
        // Randomly generate the crowd's interpersonal relationships
        for (int i=0; i<numberOfPeople; i++) {
            for (int j=0; j<numberOfPeople; j++) {
                whoKnowsWho[i][j] = random.nextBoolean();
            }
        }
        // Randomly generate x-japan index
        xjapanIndex = random.nextInt(numberOfPeople);    // 0 is the first index
        System.out.println("The identify of X-Japan is: " + xjapanIndex);
        // Make sure x-japan knows nobody else
        for (int i=0; i<numberOfPeople; i++) {
            whoKnowsWho[xjapanIndex][i] = false;
        }
        // Make sure everyone else knows x-japan
        for (int i=0; i<numberOfPeople; i++) {
            whoKnowsWho[i][xjapanIndex] = true;
        }
    }

    /**
     * Tells whether Subject knows Object
     */
    public boolean knows(int subject, int object) {
        return whoKnowsWho[subject][object];
    }

    public int getCrowdSize() {
        return numberOfPeople;
    }

}
