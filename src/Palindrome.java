public class Palindrome {

    private String text;

    public Palindrome(String text) {
        this.text = text;
    }

    public boolean testIfPalindromeWithReverse() {
        StringBuilder reverse = new StringBuilder();
        for (int i = text.length()-1; i >= 0; i--) {
            reverse.append(text.charAt(i));
        }
        if (text.equals(reverse.toString())) {
            return true;
        }
        return false;
    }

    public void testIfPalindromeWithoutReverse() {
        int beginning = 0;
        int end = text.length() - 1;
        int middle = (beginning + end) / 2;
        for (int i = beginning; i <= middle; i++) {
            if (text.charAt(beginning) == text.charAt(end)) {
                beginning++;
                end--;
            } else {
                System.out.println("Not a palindrome wow!");
                return;
            }
        }
        System.out.println("A palindrome it is!");
    }

    public void testIfPalindromeUsingRecursion(int beginning, int end) {
        if (beginning < end) {
            if (text.charAt(beginning) == text.charAt(end)) {
                testIfPalindromeUsingRecursion(beginning + 1, end - 1);
            } else {
                System.out.println("Not a palindrome!");
                return;
            }
        } else {
            System.out.println("A palindrome it is!");
        }
    }

    public static void main(String[] args) {
        Palindrome palindrome = new Palindrome("le");
        System.out.println(palindrome.testIfPalindromeWithReverse());
        palindrome.testIfPalindromeWithoutReverse();
        palindrome.testIfPalindromeUsingRecursion(0, palindrome.text.length() - 1);
    }

}
