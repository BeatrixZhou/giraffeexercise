/**
 * @author Xujia
 * A greedy, but not super smart, cute little mouse has started her venture in the stock market
 * She got hold of some inside information and knows the exact stock value of each day during a certain period
 * When should she buy and sell supposing the buying and selling can only be done once?
 */

public class MaximumStockProfit {

    private int[] stockValues;

    public MaximumStockProfit(int[] stockValues) {
        this.stockValues = stockValues;
    }

    private int[] findMaxProfit(int[] stockValues) {
        // transactionDates has only 2 elements, the buying date and the selling date
        int[] transactionDates = new int[2];
        // buying date
        transactionDates[0] = 0;
        // selling date
        transactionDates[1] = 0;
        // current min value
        int minValueIndex = 0;
        // current max gain
        int maxGain = 0;

        for (int i=0; i<stockValues.length; i++) {
            // if the new element is found to be the smallest, replace the current smallest one with the new one
            if (stockValues[i] < stockValues[minValueIndex]) {
                minValueIndex = i;
            /*the biggest gain for the new element can be acquired by subtracting the previously recorded min value
            if it is bigger than the current maxGain, replace it
            finally update the transaction dates*/
            } else if (stockValues[i]-stockValues[minValueIndex] > maxGain) {
                transactionDates[0] = minValueIndex;
                transactionDates[1] = i;
                maxGain = stockValues[i] - stockValues[minValueIndex];
            }
        }
        return transactionDates;
    }

    public static void main(String[] args) {
        int[] stockValues = {19, 3, 10, 8, 23, 99, 39, 29, 1, 5, 2, 20, 80};
        MaximumStockProfit maximumStockProfit = new MaximumStockProfit(stockValues);

        int[] transactionDates = maximumStockProfit.findMaxProfit(stockValues);

        System.out.println(String.format("Buying date: %d", transactionDates[0]));
        System.out.println(String.format("Selling date: %d", transactionDates[1]));
        System.out.println(String.format("Max gain: %d", stockValues[transactionDates[1]]-stockValues[transactionDates[0]]));
    }

}
